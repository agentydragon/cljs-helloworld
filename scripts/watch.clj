(require '[cljs.build.api :as b])

(b/watch "src"
  {:main 'cljs-helloworld.core
   :output-to "out/cljs_helloworld.js"
   :output-dir "out"})
