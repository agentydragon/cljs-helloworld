(ns cljs-helloworld.core
  (:require [clojure.browser.repl :as repl]
            [hiccup.core :as h]))

(defonce conn (repl/connect "http://localhost:9000/repl"))

(enable-console-print!)

(println "Hello world!")

; (def body (.getElementByTagName js/document "body"))
(defn avg [a b] (/ (+ a b) 2.0))

(-> (.querySelector js/document "body")
    (.innerHTML= (h/.html [:span {:class "foo"} "bar"])))


; (.render js/ReactDOM
;   (.createElement js/React "h2" nil "Hello, React!")
;   (.getElementById js/document "app"))
